// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import moment from 'moment';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'
import axios from 'axios';
import App from './App';
import router from './plugins/router';
import store from './plugins/store'
import constants from './plugins/constants';

import '@/assets/main.css'


axios.defaults.baseURL = process.env.NODE_ENV !== 'production' ? 'http://localhost:3000' : 'http://rebrotests.xyz/'

Vue.prototype.$axios = axios;
Vue.prototype.$moment = moment;
Vue.use(ElementUI, { locale });
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  data() {
    return {
      ...constants
    }
  },
  store,
  components: { App },
  template: '<App/>',
});
