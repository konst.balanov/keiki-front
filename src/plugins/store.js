import Vuex from 'Vuex'
import Vue from 'vue'
import axios from 'axios'
import moment from 'moment'

import {
  STATE_ROUTES,
  STATE_CARS,
  STATE_ROUTES_TOTAL_COUNT,
  STATE_ROUTES_PAGE_SIZE,
  STATE_CARS_TOTAL_COUNT,
  STATE_CARS_PAGE_SIZE,
  GETTERS_ROUTES,
  GETTERS_CARS,
  GETTERS_ROUTES_TOTAL_COUNT,
  GETTERS_CARS_TOTAL_COUNT,
  ACTIONS_DELETE_CAR,
  ACTIONS_DELETE_ROUTE,
  ACTIONS_GET_ROUTES,
  ACTIONS_GET_CARS,
  ACTIONS_SAVE_ROUTE,
  ACTIONS_SAVE_CAR,
  ACTIONS_UPDATE_ROUTE,
  ACTIONS_ROUTES_PAGE_SIZE_CHANGE,
  ACTIONS_CARS_PAGE_SIZE_CHANGE,
  MUTATIONS_SET_ROUTES_TOTAL_COUNT,
  MUTATIONS_SET_CARS_TOTAL_COUNT,
  MUTATIONS_UPDATE_ROUTES,
  MUTATIONS_UPDATE_CARS,
} from '@/plugins/constants.js'

Vue.use(Vuex)

console.log(STATE_ROUTES)

let store = new Vuex.Store({
  state: {
    [STATE_ROUTES]: [],
    [STATE_CARS]: [],

    [STATE_ROUTES_TOTAL_COUNT]: 0,
    [STATE_ROUTES_PAGE_SIZE]: 10,

    [STATE_CARS_TOTAL_COUNT]: 0,
    [STATE_CARS_PAGE_SIZE]: 10,
    // route: {}
  },
  getters: {
    [GETTERS_ROUTES](state) {
      return state.routes
    },
    [GETTERS_CARS](state) {
      return state.cars
    },
    [GETTERS_ROUTES_TOTAL_COUNT](state) {
      return state.total_routes_count
    },
    [GETTERS_CARS_TOTAL_COUNT](state) {
      return state.total_cars_count
    }
  },
  actions: {
    [ACTIONS_DELETE_ROUTE](context, route_id) {
      return axios.delete(`/routes/${route_id}`)
    },
    [ACTIONS_DELETE_CAR](context, car_id) {
      return axios.delete(`/cars/${car_id}`)
    },
    [ACTIONS_GET_ROUTES]({ commit, state }, { page } = { page: 0 }) {
      axios('/routes/count').then(({ data }) => commit(MUTATIONS_SET_ROUTES_TOTAL_COUNT, data.count));

      return axios("/routes", {
        params: {
          page,
          pageSize: state.routes_page_size
        }
      })
        .then(({ data }) => commit(MUTATIONS_UPDATE_ROUTES, data))
    },
    [ACTIONS_GET_CARS]({ commit, state }, { page } = { page: 0 }) {
      axios("/cars/count").then(({ data }) => commit(MUTATIONS_SET_CARS_TOTAL_COUNT, data.count));

      return axios("/cars", {
        params: {
          page,
          pageSize: state.cars_page_size
        }
      })
        .then(({ data }) => commit(MUTATIONS_UPDATE_CARS, data))
    },
    [ACTIONS_SAVE_ROUTE](context, payload) {
      let route = { ...payload };
      route.date_start = moment(route.date_start).format('YYYY-MM-DD HH:mm');
      return axios.post("/routes", route)
    },
    [ACTIONS_SAVE_CAR](context, payload) {
      let car = { ...payload };
      car.purchase_date = moment(car.purchase_date).format('YYYY-MM-DD HH:mm')
      return axios.post("/cars", car)
    },
    [ACTIONS_UPDATE_ROUTE](context, payload) {     
      let route = { ...payload };
      route.date_start = moment(route.date_start).format('YYYY-MM-DD HH:mm');

      console.log(payload)
      
      let requests = [];
      if (payload.new_car_id > 0) {
        requests.push(axios.patch(`/routes/${payload.id}/assigncar/${payload.new_car_id}`));
      }
      requests.push(axios.put(`/routes/${route.id}`, route))
      
      return axios.all(requests)
    },
    [ACTIONS_ROUTES_PAGE_SIZE_CHANGE](context, payload) {
      context.state.routes_page_size = payload;
      context.dispatch(ACTIONS_GET_ROUTES)
    },
    [ACTIONS_CARS_PAGE_SIZE_CHANGE](context, payload) {
      context.state.cars_page_size = payload;
      context.dispatch(ACTIONS_GET_CARS)
    },
  },
  mutations: {
    [MUTATIONS_SET_ROUTES_TOTAL_COUNT](state, count) {
      state.routes_total_count = count;
    },
    [MUTATIONS_SET_CARS_TOTAL_COUNT](state, count) {
      state.routes_total_count = count;
    },
    [MUTATIONS_UPDATE_ROUTES](state, payload) {
      state.routes = payload
    },
    [MUTATIONS_UPDATE_CARS](state, payload) {
      state.cars = payload
    }
  }
});

// store.dispatch(ACTIONS_GET_CAR_TYPES)
window.store = store;
export default store
