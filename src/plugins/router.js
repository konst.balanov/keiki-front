import Vue from 'vue';
import Router from 'vue-router';
import RoutesList from '@/components/RoutesList';
import CarsList from '@/components/CarsList';
import RouteInfo from '@/components/RouteInfo';
import CarInfo from '@/components/CarInfo';
import NewRoute from '@/components/NewRoute';
import NewCar from '@/components/NewCar';
import EditRoute from '@/components/EditRoute';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Routes',
      component: RoutesList,
    },
    {
      path: '/cars',
      name: 'Cars',
      component: CarsList,
    },
    {
      path: '/route/:id',
      name: 'Route',
      component: RouteInfo,
      props: (route) => { return { id: route.params.id } }
    },
    {
      path: '/car/:id',
      name: 'Car',
      component: CarInfo,
      props: (route) => { return { id: route.params.id } }
    },
    {
      path: '/newroute',
      name: 'NewRoute',
      component: NewRoute,
    },
    {
      path: '/newcar',
      name: 'NewCar',
      component: NewCar,
    },
    {
      path: '/editroute/:id',
      name: 'EditRoute',
      component: EditRoute,
      props: (route) => { return { id: route.params.id } }
    }
  ],
});
