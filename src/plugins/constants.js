module.exports = {
  STATE_ROUTES: 'routes',
  STATE_CARS: 'cars',

  STATE_ROUTES_TOTAL_COUNT: 'routes_total_count',
  STATE_ROUTES_PAGE_SIZE: 'routes_page_size',
  STATE_CARS_TOTAL_COUNT: 'cars_total_count',
  STATE_CARS_PAGE_SIZE: 'cars_page_size',

  GETTERS_ROUTES: 'routes',
  GETTERS_CARS: 'cars',
  GETTERS_ROUTES_TOTAL_COUNT: 'routes_total_count',
  GETTERS_CARS_TOTAL_COUNT: 'cars_total_count',

  ACTIONS_DELETE_ROUTE: 'deleteRoute',
  ACTIONS_DELETE_CAR: 'deleteCar',
  ACTIONS_GET_ROUTES: 'getRoutes',
  ACTIONS_GET_CARS: 'getCars',
  ACTIONS_SAVE_ROUTE: 'saveRoute',
  ACTIONS_SAVE_CAR: 'saveCar',
  ACTIONS_UPDATE_ROUTE: 'updateRoute',
  ACTIONS_ROUTES_PAGE_SIZE_CHANGE: 'routesPageSizeChange',
  ACTIONS_CARS_PAGE_SIZE_CHANGE: 'carsPageSizeChange',

  MUTATIONS_SET_ROUTES_TOTAL_COUNT: 'setRoutesTotalCount',
  MUTATIONS_SET_CARS_TOTAL_COUNT: 'setCarsTotalCount',
  MUTATIONS_UPDATE_ROUTES: 'updateRoutes',
  MUTATIONS_UPDATE_CARS: 'updateCars',
}