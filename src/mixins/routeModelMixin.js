function formTemplate() {
  return {
    id: 0,
    cityA: '',
    cityB: '',
    distance: 1,
    expected_revenue: 0,
    car_type_id: 0,
    date_start: '',
  };
}

export default {
  props: ['id'],
  data() {
    return {
      loading: true,
      route_car: null,
      highlight_autocomplete: false,
      routeForm: formTemplate(),
      rules: {
        cityA: [{ min: 1, max: 128, message: "Length should be 1 to 128" }],
        cityB: [{ min: 1, max: 128, message: "Length should be 1 to 128" }],
        expected_revenue: [{ type: 'number', min: 1 }],
        distance: [{ type: "number", min: 1 }],
        car_type_id: [{ type: "number", min: 0, max: 1, message: "Type of car is not specificated" }],
      },
    }
  },
  beforeRouteEnter(to, from, next) {
    if (to.params && to.params.fomLink) {
      this.highlight_autocomplete = true;
      setTimeout(() => this.highlight_autocomplete = false, 2000)
    }
    next();
  },
  mounted() {
    if (this.id >= 0) {
      this.loading = true;
      this.$axios(`/routes/${this.id}`)
        .then(({ data, status }) => {
          this.routeForm = data.route ? { ...data.route } : formTemplate();
          this.route_car = data.car ? { ...data.car } : null;
        })
        .catch((error) => this.$notify.error({ message: error.response.data }))
        .finally(() => this.loading = false)
    }
  },
}