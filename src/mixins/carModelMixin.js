function formTemplate() {
  return {
    id: 0,
    model: "Carea",
    registration_mark: "AX2888CC",
    purchase_date: new Date(),
    car_type_id: 1,
    mileage: 22,
  };
}

export default {
  props: ['id'],
  data() {
    return {
      loading: true,
      carForm: formTemplate(),
      car_route: null,
      rules: {
        model: [{ min: 1, max: 128, message: "Length should be 1 to 128" }],
        registration_mark: [{ min: 1, max: 128, message: "Length should be 1 to 128" }],
        purchase_date: [{ type: "date", min: 1 }],
      },
    }
  },
  mounted() {
    if (this.id)
      this.$axios(`/cars/${this.id}`)
        .then(({ data, status }) => {
          this.carForm = data.car ? { ...data.car } : formTemplate();
          this.car_route = data.route ? { ...data.route } : null;
        })
        .catch((error) => this.$notify.error({ message: error.response.data }))
        .finally(() => this.loading = false)

  },
}